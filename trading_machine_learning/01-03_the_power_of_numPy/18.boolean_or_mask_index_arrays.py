"""Boolean of mask index array"""
import numpy as np 

def test_run():
	a = np.array([(20,25,10,23,25,32,10,5,0),(0,2,50,20,0,1,28,5,0)])
	print "Array:\n", a
	print "..............................."
	#Calculating mean
	mean = a.mean()
	print "Mean: ", mean
	print "..............................."
	#masking
	print "Masking: Everything less than mean\n", a[a<mean]
	print "..............................."
	print a, "\nvs\n"
	a[a<mean] = mean
	print a
	print "..............................."

if __name__ == '__main__':
	test_run()