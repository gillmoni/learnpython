"""Slice and plot"""
from urllib import urlopen
import urllib
#from bs4 import BeautifulSoup

import os
import pandas as pd
import matplotlib.pyplot as plt


def plot_selected(df, columns, start_index, end_index):
    """Plot the desired columns over index values in the given range."""
    # TODO: Your code here
    # Note: DO NOT modify anything else!
    df.ix[start_index:end_index,columns]
    selected_data = plot_data(df.ix[start_index:end_index,columns], title = "Selected Data")
    #selected_data.set_xlabel("Date")
    #selected_data.set_ylabel("Label")

def symbol_to_path(symbol, base_dir="data"):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))


def get_data(symbols, dates):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols.insert(0, 'SPY')

    for symbol in symbols:
        #fetch_data(symbol) #Download csv for symbol loading.
        #If file exists? skip download file. WILL ADD CODE LATER#
        #FOR NOW COMMENT ABOVE LINE#
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])
    return df

def normalize_data(df):
    """Normalize stock prices using the first tow of the dataframe."""
    return df/df.ix[0,:]


def fetch_data(symbol): #Not in course. Adding (mgill)
    """ Downloads .csv files for <symbols> from Yahoo Finance and saves them in 'data' directory. These are later picked up by rese of the program."""
    
    '''url = "http://ichart.finance.yahoo.com/table.csv?s="+symbol+\
    "&amp;d=1&amp;e=1&amp;f=2016&amp;g=d&amp;a=8&amp;b=7&amp;c=2000&amp;ignore=.csv"
    '''
    time_frame = "m" # d -> daily, w -> weekly, m -> monthly.
    url = "http://real-chart.finance.yahoo.com/table.csv?s="+symbol+\
            "&a=11&b=22&c=1998&d=04&e=9&f=2016&g="+time_frame+"+&ignore=.csv"

    urllib.urlretrieve(url, './data/{}.csv'.format(symbol))
    print "DEBUG: Downloading for "+symbol
    print "DEBUG: URL:"+url

#Not using this. for now.
''' def test_run():
    # Define a date range
    dates = pd.date_range('2012-01-01', '2016-06-30')
    # Choose stock symbols to read
    #symbols = ['GOOG', 'IBM', 'GLD','AAPL']  # SPY will be added in get_data()
    symbols = ['XLY', 'XLF','XLU','XLP','XLE','XLV','XLB','XLK','XLI']  # SPY will be added in get_data()
    
    # Get stock data
    df = get_data(symbols, dates)
    df = normalize_data(df) ##This is not mentioned in tutorial

    # Slice and plot
    #plot_selected(df, ['SPY', 'IBM','GLD'], '2016-01-01', '2016-06-30')
    plot_selected(df, ['XLY', 'XLF','XLU','XLP','XLE','XLV','XLB','XLK','XLI'], '2012-01-01', '2016-06-30')
'''

def plot_data(df, title="Stock prices"):
    """Plot stock prices with a custom title and meaningful axis labels."""
    ax = df.plot(title=title, fontsize=12)
    ax.set_xlabel("Date")
    ax.set_ylabel("Price")
    plt.show()

def get_rolling_mean(values, window):
    """Return rolling mean of the given values, suing specified window size."""
    return pd.rolling_mean(values, window=window)

def get_rolling_std(values, window):
    """Return rolling standard deviation of given values, using specidied window size"""
    #TODO:
    return pd.rolling_std(values, window=window)

def get_bollinder_bands(rm, rstd, deviation_value):
    """Return upper and lower Bollinger Bands"""
    upper_band =  rm + rstd * deviation_value
    lower_band =  rm - rstd * deviation_value
    return upper_band, lower_band

def test_run():
    #Read data
    dates = pd.date_range('2016-01-01','2016-12-31')
    symbols  = ['SPY']
    df = get_data(symbols, dates)

    rm_SPY = get_rolling_mean(df['SPY'], 20)    #Computer rolling mean using a 20-day window
    rstd_SPY = get_rolling_std(df['SPY'], 20)  #Compute rolling std deviation.
    upper_band, lower_band = get_bollinder_bands(rm_SPY, rstd_SPY, 2)

    #Plot raw SPY values, rolling mean and Bollinder bands.
    ax = df['SPY'].plot(title="SPY rolling mean", label = "SPY")
    rm_SPY.plot(label='Bollinder Bands', ax=ax)    #Adding rolling mean to the same plot
    
    upper_band.plot(label="Upper Band", ax=ax)
    lower_band.plot(label="Lower Band", ax=ax)
    
    #Add axis labels and legend
    ax.set_xlabel("Date")
    ax.set_ylabel("Price")
    ax.legend(loc='upper left')

    plt.show()

if __name__ == "__main__":
    test_run()