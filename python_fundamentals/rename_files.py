import os
def rename_files():
	#(1) get file names from a folder.
	file_list = os.listdir("/home/mgill/Downloads/prank")
	#print (file_list)
	
	saved_path = os.getcwd()
	print ("Current working directory is "+saved_path)

	os.chdir("/home/mgill/Downloads/prank/")
	print ("New working path is "+os.getcwd())
	#(2) for eachfile, rename file.
	for file_name in file_list:
		print ("Old Name - "+file_name , "---> New Name - "+file_name.translate(None,"0123456789"))
		os.rename(file_name,  file_name.translate(None,"0123456789"))
	
	os.chdir(saved_path)
	print ("Restored working path to "+os.getcwd())


rename_files()