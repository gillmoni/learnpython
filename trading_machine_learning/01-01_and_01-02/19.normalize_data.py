"""Slice and plot"""

import os
import pandas as pd
import matplotlib.pyplot as plt


def plot_selected(df, columns, start_index, end_index):
    """Plot the desired columns over index values in the given range."""
    # TODO: Your code here
    # Note: DO NOT modify anything else!
    df.ix[start_index:end_index,columns]
    selected_data = plot_data(df.ix[start_index:end_index,columns], title = "Selected Data")
    #selected_data.set_xlabel("Date")
    #selected_data.set_ylabel("Label")

def symbol_to_path(symbol, base_dir="data"):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))


def get_data(symbols, dates):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols.insert(0, 'SPY')

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                parse_dates=True, usecols=['Date', 'Adj Close'], na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':  # drop dates SPY did not trade
            df = df.dropna(subset=["SPY"])

    return df

def normalize_data(df):
    """Normalize stock prices using the first tow of the dataframe."""
    return df/df.ix[0,:]

def plot_data(df, title="Stock prices"):
    """Plot stock prices with a custom title and meaningful axis labels."""
    ax = df.plot(title=title, fontsize=12)
    ax.set_xlabel("Date")
    ax.set_ylabel("Price")
    plt.show()


def test_run():
    # Define a date range
    dates = pd.date_range('2016-01-01', '2016-06-30')
    # Choose stock symbols to read
    symbols = ['GOOG', 'IBM', 'GLD','AAPL']  # SPY will be added in get_data()
    
    # Get stock data
    df = get_data(symbols, dates)
    df = normalize_data(df) ##This is not mentioned in tutorial

    # Slice and plot
    plot_selected(df, ['SPY', 'IBM','GOOG','GLD'], '2016-01-01', '2016-06-30')


if __name__ == "__main__":
    test_run()
