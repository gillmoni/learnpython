"""Array attributes"""
import numpy as np 

def test_run():
	a = np.random.random((5,4)) 	#5x4 array of random numbers
	print a 						#Print contents.

	print a.shape					#Shape of an array.
	print a.shape[0] 				#number of rows
	print a.shape[1] 				#number of columns

	print len(a.shape) 				#Length of the tuple (dimension of the array)

	print a.size 					#Number of elements in array.

	b = np.empty((5,4,3), dtype=np.int)	#Number of elements in a 3D array.
	print b.size 

	#Check data_type of elements.
	print a.dtype			

if __name__ == '__main__':
	test_run()
