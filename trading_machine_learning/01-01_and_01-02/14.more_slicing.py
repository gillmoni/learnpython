"""Utility functions"""

import os
import pandas as pd

def symbol_to_path(symbol, base_dir="data"):
    """Return CSV file path given ticker symbol."""
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))


def get_data(symbols, dates):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    df = pd.DataFrame(index=dates)
    if 'SPY' not in symbols:  # add SPY for reference, if absent
        symbols.insert(0, 'SPY')

    for symbol in symbols:
        # TODO: Read and join data for each symbol
        df_temp = pd.read_csv(symbol_to_path(symbol), index_col='Date',
                    parse_dates=True,
                    usecols=['Date','Adj Close'], na_values =['nan'])

        df_temp = df_temp.rename(columns={'Adj Close':symbol})
        df_temp = df_temp.rename(columns={'Date':symbol})
        df = df.join(df_temp)
        
        #df = df.dropna() #Drop NaN values. OR use
        if symbol == 'SPY': #Drop dates SPY did not trade.
            df = df.dropna(subset=["SPY"])
    return df


def test_run():
    # Define a date range
    dates = pd.date_range('2010-01-01', '2010-12-31')

    # Choose stock symbols to read
    symbols = ['GOOG', 'IBM', 'GLD']
    
    # Get stock data
    df = get_data(symbols, dates)
    #print df

    #Slice by row range (dates) using DataFrame.ix[] selector
    #print df.ix['2010-01-01':'2010-01-31'] #Month of January
    
    #Slice by column (symbols)
    #print df['GOOG'] #For GOOG only.
    #print df[['GOOG','IBM']] #For GOOG and IBM only.
    
        
    #Slice by column and dates
    print df.ix['2010-01-01':'2010-01-15',['SPY','IBM']]


if __name__ == "__main__":
    test_run()
