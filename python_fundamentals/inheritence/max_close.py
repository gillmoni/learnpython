import pandas as pd 

def get_max_close(symbol):
	"""Returns the max close value for a stock indicated by symbol.
	Note: Data for a a stock is store in file: data/<symbol>.csv
	"""
	df = pd.read_csv("data/{}.csv".format(symbol)) #Read in data
	return df['Close'].max() #Compute return max.

def test_run():
	"""Function called by Test Run """
	for symbol in ['AAPL','IBM']:
		print "Max Close"
		print symbol, get_max_close(symbol)

if __name__ == "__main__":   #if run standalone.
	test_run()