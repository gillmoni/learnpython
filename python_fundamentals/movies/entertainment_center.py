import fresh_tomatoes
import media

toy_story = media.Movie("Toy Story", "A story of a boy and his toys",
						"http://www.gstatic.com/tv/thumb/movieposters/17420/p17420_p_v8_ab.jpg",
						"https://www.youtube.com/watch?v=KYz2wyBy3kc")
#print (toy_story.storyline)
avatar = media.Movie("Avatar", "A marine on an alien planet",
					 "http://t0.gstatic.com/images?q=tbn:ANd9GcQCfmvrE4fMo2cd8esc7mDZPtFSJThAujddMPkRtti1_ij6u-jp",
					 "https://www.youtube.com/watch?v=cRdxXPV9GNQ")
#print (avatar.storyline)
#avatar.show_trailer()

shawshank_redemption = media.Movie("The Shawshank Redemption",
								   "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency" ,
								   "http://ecx.images-amazon.com/images/I/519NBNHX5BL.jpg",
								   "https://www.youtube.com/watch?v=6hB3S9bIaco")
#shawshank_redemption.show_trailer()

movies = [toy_story, avatar, shawshank_redemption]
#fresh_tomatoes.open_movies_page(movies)

#print (media.Movie.VALID_RATINGS)
print (media.Movie.__doc__)
print (media.Movie.__name__)
print (media.Movie.__module__)