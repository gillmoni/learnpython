import datetime 			#To get yesterday's date.
import pandas as pd 		
import numpy as np
import matplotlib.pyplot as plt
import pandas.io.data as web
import quandl
import time

###################################################################
start_time = time.time()

#gl_symbols = ['^GSPC',]					#YahooFinance Only
#gl_fred_symbols = ['NAPM','PERMITS','NMFCI']	#Fred ONLY
gl_quandl_symbols = ['CHRIS/CME_ES1','CHRIS/CME_NQ1','CHRIS/CME_HG1','CHRIS/CME_CL1','FRED/NAPM', 'WIKI/AAPL'] 
#gl_quandl_symbols = ['CHRIS/CME_ES1','CHRIS/CME_NQ1','CHRIS/CME_HG1','CHRIS/CME_CL1','CURRFX/USDEUR','CURRFX/GBPJPY','FRED/NAPM', 'WIKI/AAPL'] 

gl_start_date = '01/01/2008'
gl_end_date=datetime.datetime.now() - datetime.timedelta(days=1)  #Yesterday
gl_normalize = 'yes' #[yes,no]
###################################################################
###################################################################
def validate_stock_symbol(stock_to_be_validated):
	try:
		#If only a string is specified. This is shorthand for AAPL, e.g WIKI/AAPL.
		#If so, add WIKI to the input passed.
		if stock_to_be_validated.find('/') < 0: 
 			value_input = "WIKI/"+stock_to_be_validated
 		else:
 			value_input=stock_to_be_validated
 	except:
 		print "validate_stock_symbol: Invalid stock symbol passed."

 	return value_input
###################################################################
def find_source_of_contract(value_input):
	try:	
		input_contract = validate_stock_symbol(value_input)
		qdl_source, qdl_combo = input_contract.split("/")
		if qdl_source == 'CHRIS':
			qdl_exchange, qdl_contract = qdl_combo.split("_")
		else:
			qdl_contract = qdl_combo
			qdl_exchange = qdl_source
	except:
		print "find_source_of_contract: Couldn't find source."
	#print "find_source_of_contract: ", qdl_source	
	return qdl_source
###################################################################		
def find_data_series(value_input):
	try:
		qdl_source = find_source_of_contract(value_input)
		#Validate symbol source, and assign values.
		if qdl_source == 'CHRIS':	#For Futures
			ax_data_series='Settle'
		if qdl_source == 'WIKI':	#For Stocks
			ax_data_series='Adj. Close'
		if qdl_source == 'FRED':	#For Economic indicators
			ax_data_series='VALUE'
		if qdl_source == 'CURRFX':	#For Currencies.
			ax_data_series='Rate'
	except:
		print "find_data_series: data_series not found"
	
	#print "find_data_series:data_series> ", ax_data_series
	return ax_data_series
###################################################################	
def get_data(symbol, *dates):
	"""Get data from Yahoo finance only"""
	if len(dates) is not 0: 
		#If dates are passed, transfer values from variable arguments.
		gd_start_date=dates[0] 
		gd_end_date=dates[1]
	else: 
		#Select start dates as 01/01/2015 and gd_end_date as yesterdays date.
		gd_start_date='01/01/2016'
		gd_end_date=datetime.datetime.now() - datetime.timedelta(days=1)  #Yesterday
	try:
		df = web.DataReader(symbol, 'yahoo', start=gd_start_date, end=gd_end_date)
		df = df.rename(columns={"Adj Close": "Adj. Close"})
		#print df.tail(1)
	except:
		print ("get_data: Can't get_data for "+symbol)
	
	if gl_normalize is 'yes':
		return normalize_data(df)		#If comparing stocks as normalized data.
	else:
		return df 						#If keeping default values.
###################################################################
def get_fred_data(fred_symbol, *dates):
	"""Get data from FRED only"""
	if len(dates) is not 0: 
		#If dates are passed, transfer values from variable arguments.
		gd_start_date=dates[0] 
		gd_end_date=dates[1]
	else: 
		#Select start dates as 01/01/2015 and gd_end_date as yesterdays date.
		gd_start_date='01/01/2016'
		gd_end_date=datetime.datetime.now() - datetime.timedelta(days=1)  #Yesterday
	try:
		df_fred = web.DataReader(fred_symbol, 'fred', start=gd_start_date, end=gd_end_date)
		df_fred = df_fred.rename(columns={fred_symbol: "VALUE"}) #To avoid error in plot_spread function with 'fred' as source.
	except:
		print ("get_fred_data: Can't get_fred_data for "+fred_symbol)
	if gl_normalize is 'yes':
		return normalize_data(df_fred)		#If comparing stocks as normalized data.
	else:
		return df_fred 						#If keeping default values.
###################################################################
def get_quandl_data(quandl_symbol, *dates):
	"""Get data from Quandl, works for different types of data sets."""
	quandl.ApiConfig.api_key = "8uCyKtAss6c4Ys-P5SP9"
	if len(dates) is not 0: 
		#If dates are passed, transfer values from variable arguments.
		gd_start_date=dates[0] 
		gd_end_date=dates[1]
	else: 
		#Select start dates as 01/01/2015 and gd_end_date as yesterdays date.
		gd_start_date='01/01/2016'
		gd_end_date=datetime.datetime.now() - datetime.timedelta(days=1)  #Yesterday
	
	#quandl_symbol = validate_stock_symbol(quandl_symbol)

	try:
		quandl_symbol = validate_stock_symbol(quandl_symbol)
		#Get full data frame, no need to specific table.
		df_quan = quandl.get(quandl_symbol,start_date=gd_start_date, end_date=gd_end_date)
	
	except:
		print ("get_quandl_data: Can't get_quandl_data for "+quandl_symbol)

	if gl_normalize is 'yes':
		return normalize_data(df_quan)		#If comparing stocks as normalized data.
	else:
		return df_quan 	
######################################################################################################################################
def plot_data(symbol, *dates):
	try:
		ax = get_data(symbol, *dates)['Adj Close'].plot(label=symbol)
		plt.legend( loc='upper center')
		return ax
	except:
		print "plot_data: Can't plot_data for ", symbol
###################################################################
def plot_fred_data(fred_symbol, *dates):
	try:
		ax_fred = get_fred_data(fred_symbol, *dates)[fred_symbol]
		ax_fred.plot(label=fred_symbol)
		plt.legend( loc='upper center')
		return ax_fred
	except:
		print ("plot_fred_data: Can't plot_fred_data for "+fred_symbol)
###################################################################
def plot_moving_average(quandl):
	#Plot moving average.
	#ax_quan['42d'] = pd.rolling_mean(ax_quan[ax_data_series], window=42) 
	#ax_quan['42d'].plot(label='42d') #Plot moving average.
	ax_quan = get_quandl_data(quandl_symbol, *dates)
###################################################################	
def plot_quandl_data(quandl_symbol, *dates):
	qdl_source = find_source_of_contract(quandl_symbol)
	data_series = find_data_series(quandl_symbol)
	try:
		ax_quan = get_quandl_data(quandl_symbol, *dates)
		ax_quan[data_series].plot(label=quandl_symbol)	
	except:
		print ("plot_quandl_data: Can't plot_quandl_data for "+quandl_symbol)
	
	plt.legend( loc='upper left')
	return ax_quan
######################################################################################################################################
######################################################################################################################################
def plot_spread(source, symbol1, symbol2, *dates):
	data_series1 = find_data_series(symbol1)
	data_series2 = find_data_series(symbol2) 
	
	try:
		if source == 'yahoo':
			df_spread = get_data(symbol2, *dates)/get_data(symbol1, *dates)
		if source == 'fred':
			data_series1 = "VALUE"
			df_spread = get_fred_data(symbol2, *dates)/get_fred_data(symbol1, *dates)
		else:	#If not yahoo/fred, then get data from quandl
			df_spread = get_quandl_data(symbol2, *dates)/get_quandl_data(symbol1, *dates)			
	except:
		print "plot_spread: Invalid data source specified.[yahoo/fred/quandl]"		
		
	#try:
	df_spread[data_series1].plot(label=symbol2+"/"+symbol1)
	plt.legend( loc='upper center', numpoints = 1 )
	#except:
	#	print ("plot_spread: Can't plot_spread for "+symbol1,symbol2)
###################################################################
def spread_result(symbol1, symbol2):
	try:
		df_spread = get_data(symbol2)['Adj Close']/get_data(symbol1)['Adj Close']
		print (df_spread.tail(2))
	except:
		print ("spread_result: Can't get spread_result for "+symbol2+"/"+symbol1)
###################################################################
def normalize_data(df):
	"""Normalize stock prices using the first row of the dataframe."""
	return df/df.ix[0,:]
###################################################################
def calculate_print_math_values(daily_returns):

	#Get mean and std deviation
	print "Mean: ", daily_returns.mean()
	print "Kurtosis: ", daily_returns.kurtosis()
	print "Skewness: ", daily_returns.skew()
	print "Minimum: ", daily_returns.min()
	print "Maximum: ", daily_returns.max()
	print "Sum: ", daily_returns.sum()
	print "Std Dev: ", daily_returns.std()
	print "Variance: ", daily_returns.var()
	print "Count: ", daily_returns.count()
###################################################################
def compute_daily_returns(symbol, *dates):
	"""Compute and return the daily return values"""
	try:
		#cdr_df = get_data(symbol)
		cdr_df = get_quandl_data(symbol, gl_start_date, gl_end_date)
			
		daily_returns = cdr_df.copy()
		daily_returns[1:]=(cdr_df[1:]/cdr_df[:-1].values) - 1
		daily_returns.ix[0,:] = 0
		return daily_returns
	except:
		print "compute daily_returns: can't finish compute_daily_returns."

	#print "Printing daily_returns", daily_returns.tail(2)	
###################################################################
def plot_distribution(symbol, *dates):
	"""Plot distribution for a symbol"""
	data_series = find_data_series(symbol)
	try:
		#Compute daily returns
		daily_returns = compute_daily_returns(symbol)[data_series]
		mean = daily_returns.mean()
		std = daily_returns.std()
		
		#Plot a histogram
		#plt.title(symbol)
		print "Histogram:", symbol
		plt.xlabel('value')
		plt.ylabel('frequency')
		daily_returns.hist(bins=60, label=symbol)
		plt.axvline(mean, color = 'w', linestyle = 'dashed', linewidth = 1)
		plt.axvline(std, color = 'r', linestyle = 'dashed', linewidth = 1)
		plt.axvline(-std, color = 'r', linestyle = 'dashed', linewidth = 1)		

	except:
		print "plot_distribution: can't plot data."

	plt.legend(loc=0)
	plt.grid(True)
	#plt.show()			#Uncomment to show separate histogram for every pairself.
	calculate_print_math_values(daily_returns)

	return daily_returns
###################################################################

def test_run():	
	#Calculate date range
	dates=pd.date_range(gl_start_date,gl_end_date)
	try:
		for symbol in gl_symbols:
			plot_data(symbol, gl_start_date, gl_end_date)
			#plot_data(symbol)		#If calling with default dates.
	except:
		#print "gl_symbols: undefined."  ; 
		pass

	try:
		for fred_symbol in gl_fred_symbols:
			plot_fred_data(fred_symbol, gl_start_date, gl_end_date)
	except:
		#print "gl_fred_symbols: undefined."  ; 
		pass

	try:
		for quandl_symbol in gl_quandl_symbols:
			plot_quandl_data(quandl_symbol, gl_start_date, gl_end_date)	
	except:
		#print "gl_quandl_symbols: undefined."  ; 
		pass
	 			
	print("--- %s seconds ---" % (time.time() - start_time))
	plt.show() #Show graphs.

###################################################################
if __name__ == '__main__':
	#test_run()
	#spread_result('MSFT', 'AAPL')
	#plot_spread('quandl', 'AAPL', 'CSCO', gl_start_date, gl_end_date)  #Calling with dates parameter.
	#plot_spread('fred', 'NAPM', 'NMFCI', gl_start_date, gl_end_date)  	#Calling with dates parameter.
	#plot_spread('quandl', 'CURRFX/GBPJPY', 'CURRFX/USDEUR', gl_start_date, gl_end_date)  	#Calling with dates parameter.
	
	#plot_distribution('CURRFX/GBPJPY', gl_start_date, gl_end_date)  	#Calling with dates parameter.
	#plot_distribution('WIKI/AAPL', gl_start_date, gl_end_date)  	#Calling with dates parameter.
	plot_distribution('CHRIS/CME_ES1', gl_start_date, gl_end_date)  	#Calling with dates parameter.
	plt.show()
