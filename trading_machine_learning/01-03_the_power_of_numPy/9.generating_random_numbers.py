"""Create NumPy arrays."""

import numpy as np 
def test_run():
	#Generate an array full of random numbers, uniformly samples from [0.0 to 1.0]
	#print np.random.random((5,4)) 				# pass in a size tuple.

	#Another variation is using rand.
	#print np.random.rand(5,4) 					#Function agruments not a tuple.

	#A normal sample distribution
	print np.random.normal(size=(2,3))
	print np.random.normal(50,10, size=(2,3)) 	#Change the mean to 50 and s.d to 10.

	#Geneate Random Intergers
	print np.random.randint(10) 				#a single integer in [0,10]
	print np.random.randint(0, 10)				#same as above, specifying [low, high] explicit.
	print np.random.randint(0, 10, size=5)		#5 random intergers as 1D array.
	print np.random.randint(0, 10, size=(2,3))	#2x3 array of random intergers.

if __name__ == '__main__':
	test_run()