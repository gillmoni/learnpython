"""Scatterplots	"""
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np

from util import get_data, plot_data

def compute_daily_returns(df):
	"""Compute and return the daily return values"""
	daily_returns = df.copy()
	daily_returns[1:]=(df[1:]/df[:-1].values) - 1
	daily_returns.ix[0,:] = 0
	return daily_returns

def test_run():
	#Read Data
	dates = pd.date_range('2009-01-01', '2012-12-31')
	symbols = ['SPY','XOM', 'GLD']
	df = get_data(symbols, dates)
	plot_data(df)
	
	#Compute daily returns
	daily_returns = compute_daily_returns(df)
	plot_data(daily_returns)
	
	
	daily_returns.plot(kind='scatter', x='SPY', y='XOM') #Scatterplot SPY vx XOM
	beta_XOM, alpha_XOM = np.polyfit(daily_returns['SPY'], daily_returns['XOM'], 1)
	plt.plot(daily_returns['SPY'], beta_XOM*daily_returns['SPY'] + alpha_XOM, '-',color='r')
	print "XOM Alpha: ", alpha_XOM
	print "XOM Beta: ", beta_XOM
	plt.show()


	#daily_returns.plot(kind='scatter', x='SPY', y='GLD') #Scatterplot SPY vx GLD
	#beta_GLD, alpha_GLD = np.polyfit(daily_returns['SPY'], daily_returns['GLD'], 1)
	#plt.plot(daily_returns['SPY'], beta_GLD*daily_returns['SPY'] + alpha_GLD, '-',color='r')
	#print "GLD Alpha: ", alpha_GLD
	#print "GLD Beta: ", beta_GLD
	#plt.show()
	
if __name__ == '__main__':
	test_run()