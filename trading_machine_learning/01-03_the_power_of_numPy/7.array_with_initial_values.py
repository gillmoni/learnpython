"""Creating NumPY arrays"""

import numpy as np 

def test_run():
	#Empty array
	print np.empty(5) #1D array
	print np.empty((5,4)) #2D array
	#print np.empty((5,4,3)) #3D array
	
	#Create an array full of ONE's.
	print np.ones((5,4)) #5x4 2D array with 1's.
	
	#Create an array full of ZERO's.
	print np.zeros((5,4)) #5x4 2D array with 0's.

if __name__ == '__main__':
	test_run()