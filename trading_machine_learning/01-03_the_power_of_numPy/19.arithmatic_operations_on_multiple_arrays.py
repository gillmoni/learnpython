"""Arithmatic operations"""

import numpy as np

def test_run():
	a = np.array([(1,2,3,4,5),(10,20,30,40,50)])
	print "Original array a:\n", a

	b = np.array([(100,200,300,400,500),(1,2,3,4,5)])
	print "Original array b\n", b

	#Shape of A & B should be similar.
	print "Add: a+b: \n", a+b 			#Add
	print "Sub: a-b: \n", a-b 			#Subtract

	#Multiplication (elements of array)
	print "Mul: a*b: \n", a*b

	#Division
	print "Div: a/b: \n", a/b



if __name__ == '__main__':
	test_run()