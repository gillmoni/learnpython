import turtle

def draw_triangle (some_turtle):
	length = 100
	angle = 60
	#while (length > 0):
	for x in xrange(1,2):
			some_turtle.forward(length)
			some_turtle.right(angle)
			some_turtle.backward(length)
			some_turtle.right(angle)
			some_turtle.forward(length)
			some_turtle.right(angle)
			some_turtle.backward(length/2 )
			draw_triangle_reverse(some_turtle, length)
			

def draw_triangle_reverse (some_turtle, old_length):
	angle = 60
	length = old_length / 2
	#while (length != 1):
	for x in xrange(1,2):
			some_turtle.left(angle*2)
			some_turtle.backward(length/20)
			some_turtle.backward(length)
			#some_turtle.left(angle)
			#some_turtle.forward(length)
			#some_turtle.left(angle)
			#some_turtle.backward(length)
			#print (length)

			
def draw_art ():
	window = turtle.Screen()
	window.bgcolor("white")

	#Create the turtle Brad - Draws a triangle.
	brad = turtle.Turtle()
	brad.speed(1)
	brad.shape("turtle")
	brad.color("black")
	draw_triangle(brad)
	
	#brad.right(90)
	#brad.forward(200)
	#brad.right(180)
	#brad.forward(200)
	#Create Turtle Angie - Draws a circle
	#angie = turtle.Turtle()		
	#angie.shape("arrow")
	#angie.color("blue")
	#angie.circle(50)
	
	#sleep(10)
	window.exitonclick()
	#window.exit()

draw_art()