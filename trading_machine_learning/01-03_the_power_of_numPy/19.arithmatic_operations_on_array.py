"""Arithmatic operations"""

import numpy as np

def test_run():
	a = np.array([(1,2,3,4,5),(10,20,30,40,50)])
	print "Original array:\n", a

	#Multiply by 2
	print "\nMultiply a by 2:\n", 2* a

	#Multiply RowX by 2.
	print "\nMultiply row 1 by 2:\n", 2* a[0]
	#print "\nMultiply row 1 by 2:\n", 2* a[:1]

	#Divide RowX by 2.
	print "\nDivide row 1 by 2:\n", a/2			#Divided by integer.
	print "\nDivide row 1 by 2.0:\n", a/2.0		#Divided by float


if __name__ == '__main__':
	test_run()