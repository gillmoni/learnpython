import turtle

def draw_square (some_turtle):
	for y in xrange(0,36):
		for x in xrange(1,5):
			some_turtle.forward(100)
			some_turtle.right(90)
		some_turtle.right(10)	
def draw_art ():
	window = turtle.Screen()
	window.bgcolor("white")

	#Create the turtle Brad - Draws a square.
	brad = turtle.Turtle()
	brad.speed(2000)
	brad.shape("turtle")
	brad.color("black")
	draw_square(brad)
	
	brad.right(90)
	brad.forward(200)
	brad.right(180)
	brad.forward(200)
	#Create Turtle Angie - Draws a circle
	#angie = turtle.Turtle()		
	#angie.shape("arrow")
	#angie.color("blue")
	#angie.circle(50)
	
	window.exitonclick()
	#sleep(5)
	#window.exit()

draw_art()