"""Operations on arrays"""

import numpy as np 

def test_run():
	np.random.seed(693) 								#seed the random number generator
	a = np.random.randint(0,10, size=(5,4))				#5x4 random integers in [0,10]
	print "Array:\n", a

	#Sum of all the elements
	print "Sum of all emenets:", a.sum()

	#Sum along direction (rows or colums)
	#axis = 0 ; rows, axis = 1 ; colums
	print "Sum of each column:\n", a.sum(axis=0)
	print "Sum of each row:\n", a.sum(axis=1)

	#Statistics: min, max, mean (across rows, cols and overall)
	print "Maximum of each column:\n", a.min(axis=0)
	print "Maximum of each row:\n", a.max(axis=1)
	print "Mean of all elements:\n", a.mean()			#Leave out axis argument.

	#Get mean along each axis.
	print "Mean of all col elements:\n", a.mean(axis=0) #Mean of column elements
	print "Mean of all row elements:\n", a.mean(axis=1) #Mean of row elements

if __name__ == '__main__':
	test_run()