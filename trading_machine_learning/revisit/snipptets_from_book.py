import datetime 			#To get yesterday's date.
import pandas as pd 		
import numpy as np
import matplotlib.pyplot as plt
import pandas.io.data as web
###################################################################
#gl_symbols = ['CIEN','CSCO','AAPL', 'MSFT']	
gl_symbols = ['CIEN','CSCO']	
gl_normalize = 'yes' #[yes,no]
gl_start_date = '03/14/2012'
gl_end_date=datetime.datetime.now() - datetime.timedelta(days=1)  #Yesterday
###################################################################	
def get_data(symbol, *dates):
	if len(dates) is not 0: 
		#If dates are passed, transfer values from variable arguments.
		gd_start_date=dates[0] 
		gd_end_date=dates[1]
	else: 
		#Select start dates as 01/01/2015 and gd_end_date as yesterdays date.
		gd_start_date='01/01/2016'
		gd_end_date=datetime.datetime.now() - datetime.timedelta(days=1)  #Yesterday
	try:
		df = web.DataReader(symbol, 'yahoo', start=gd_start_date, end=gd_end_date)
	except:
		print "get_data: Can't get_data for "+symbol
		#print df.tail(1)
	if gl_normalize is 'yes':
		return normalize_data(df)		#If comparing stocks as normalized data.
	else:
		return df 						#If keeping default values.
###################################################################
def plot_data(symbol, *dates):
	try:
		ax = get_data(symbol, *dates)['Adj Close'].plot(label=symbol)
		plt.legend( loc='upper center')
	except:
		print "plot_data: Can't plot_data for "+symbol

###################################################################
def plot_spread(symbol1, symbol2, *dates):
	try:	
		df_spread = get_data(symbol2, *dates)['Adj Close']/get_data(symbol1, *dates)['Adj Close']
		df_spread.plot(label=symbol2+"/"+symbol1)
		#plt.plot(df_spread, label=symbol2+"/"+symbol1)
		plt.legend( loc='upper center', numpoints = 1 )
		plt.show()
	except:
		print "plot_spread: Can't plot_spread for "+symbol1,symbol2

###################################################################
def spread_result(symbol1, symbol2):
	try:
		df_spread = get_data(symbol2)['Adj Close']/get_data(symbol1)['Adj Close']
		print df_spread.tail(2)
	except:
		print "spread_result: Can't get spread_result for "+symbol2+"/"+symbol1
###################################################################
def normalize_data(df):
	"""Normalize stock prices using the first tow of the dataframe."""
	return df/df.ix[0,:]

###################################################################
def test_run():	
	dates=pd.date_range(gl_start_date,gl_end_date)
	for symbol in gl_symbols:
		plot_data(symbol, gl_start_date, gl_end_date)
		#plot_data(symbol)		#If calling with default dates.
	plt.show()

###################################################################
if __name__ == '__main__':
	test_run()
	#spread_result('MSFT', 'AAPL')
	plot_spread('CIEN', 'CSCO', gl_start_date, gl_end_date)  	#Calling with dates parameter.
	#plot_spread('CIEN', 'CSCO')  	#Calling without dates parameter.
	#plot_spread('GOOG', 'AMZN', gl_start_date, gl_end_date)  	#Calling with dates parameter.
	