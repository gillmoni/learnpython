import pandas as pd 

def get_max_close(symbol):
	"""Returns the max close value for a stock indicated by symbol.
	Note: Data for a a stock is store in file: data/<symbol>.csv
	"""
	#df = pd.read_csv("data/{}.csv".format(symbol)) #Read in data
	df = pd.read_csv("data/"+symbol+".csv") 		#Read in data
	return df['Close'].max() #Compute return max.

def get_mean_volume(symbol):
	"""
	Note: Data for stock is stored in file: data/<symbol>.csv
	"""
	df = pd.read_csv("data/{}.csv".format(symbol)) # read in data.
	return df['Volume'].mean() #Compute volume mean.

def test_run():
	"""Function called by Test Run """
	for symbol in ['AAPL','IBM']:
		#print "Max Close"
		#print symbol, get_max_close(symbol)
		print "................................"
		print "Mean volume"
		print symbol, get_mean_volume(symbol)


if __name__ == "__main__":   #if run standalone.
	test_run()

#http://real-chart.finance.yahoo.com/table.csv?s=IBM&a=11&b=1&c=2008&d=04&e=9&f=2016&g=d&ignore=.csv