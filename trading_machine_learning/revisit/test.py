from urllib import urlopen
import urllib
import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt

def symbol_to_path(symbol, base_dir="."):
	return os.path.join(base_dir, "{}.txt".format(str(symbol)))

def get_data(symbols, dates):
	df = pd.DataFrame(index=dates)

	for symbol in symbols:
		#fetch_data(symbol)
		df_temp = pd.read_csv(symbol_to_path(symbol),skiprows = 0, parse_dates = True, index_col= "DATE")
		#df_temp = df_temp.rename(columns={'VALUE':symbol})
		df = df.join(df_temp)
		
		#df_temp.plot()
		print df_temp.columns

		plt.show()

	return df

def fetch_data(symbol): #Not in course. Adding (mgill)
    """ Downloads .csv files for <symbols> from Yahoo Finance and saves them in 'data' directory. These are later picked up by rese of the program."""
    url = 'https://research.stlouisfed.org/fred2/data/'+symbol+'.txt'
    urllib.urlretrieve(url, './{}.txt'.format(symbol))
    print "DEBUG: Downloading for "+symbol
    print "DEBUG: URL:"+url

def test_run():
	dates = pd.date_range('2016-05-16','2010-01-01')
	symbols = ['PERMIT']
	df = get_data(symbols, dates)

if __name__ == '__main__':
	test_run()