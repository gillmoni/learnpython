"""Accessing array elements"""

import numpy as np 

def test_run():
	a = np.random.rand(5,4)
	print "Array:\n", a
	#Accessing a value to a particular location
	a[0,0] = 1;
	print "\nModified (replaced one element):\n", a

	#Assignning a list to a column in an array
	a[:,3] = [1,2,3,4,5]
	print "\nModified (replaced one column with a list):\n", a

if __name__ == '__main__':
	test_run()