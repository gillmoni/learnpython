"""Accessing array elements."""

import numpy as np 

def test_run():
	#a = np.random.randint(5,4))
	a = np.random.randint(0, 5, size=(5,4))
	#print np.random.randint(0, 10, size=(2,3))
	print "Array:\n", a
	print "............................."
	#Accessing element at position (3,2)
	element = a[3,2]
	print element
	print "............................."
	#Accessing elements in ranges.
	print a[0,1:3] 		#For row zero, 1st to 3rd column, excluding third col.
	print "............................."
	#Top-left corner
	print a[0:2,0:2]
	print "............................."	
	#Note: Slice n:m:t specifies a range that starts at n, and stops before m,
	#in steps of size t.
	#This prints col1 and col2 with all the rows.
	print a[:,0:3:2]
	print "............................."


if __name__ == '__main__':
	test_run()