import pandas as pd

def test_run():
	df = pd.read_csv("data/AAPL.csv")
	#print df.head(5) 	#Print entire dataframe.
	print df[10:21]		#Print row range 10 to 20
	print "...............\nPrint only Adj Close\n..............."
	print df['Close']	#Print Adj Close col

if __name__ == "__main__":
	test_run()